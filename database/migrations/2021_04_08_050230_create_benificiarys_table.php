<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBenificiarysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benificiaries', function (Blueprint $table) {
            $table->id();
            $table->string('user_id', 255);
            $table->string('application_id', 255);
            $table->string('nama_waris', 255);
            $table->string('hubungan_waris', 255);
            $table->string('kad_pengenalan_waris', 255);
            $table->string('telefon', 255);
            $table->string('alamat_waris', 255);
            $table->string('attachment')->nullable();
            $table->string('attachment_img_stored_path')->nullable();
            $table->string('attachment_img_mime_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benificiaries');
    }
}
