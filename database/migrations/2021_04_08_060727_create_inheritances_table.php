<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInheritancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inheritances', function (Blueprint $table) {
            $table->id();
            $table->string('user_id', 255);
            $table->string('application_id', 255);
            $table->string('no_hak_milik', 255);
            $table->string('no_lot', 255);
            $table->string('alamat', 255);
            $table->string('attachment')->nullable();
            $table->string('attachment_img_stored_path')->nullable();
            $table->string('attachment_img_mime_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inheritances');
    }
}
