<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('applications', function (Blueprint $table) {
            $table->id();
            $table->string('user_id', 255);
            $table->string('reference_id', 255);
            $table->enum('status', ['belum disemak', 'lengkap', 'tidak lengkap', 'dalam proses', 'pembicaraan', 'selesai']);
            $table->string('taraf_kepentingan', 255);
            $table->string('nama_pemohon', 255);
            $table->string('hubungan_pemohon', 255);
            $table->string('kad_pengenalan_pemohon', 255);
            $table->string('alamat_pemohon', 255);
            $table->string('telefon', 255);
            $table->string('nama_simati', 255);
            $table->string('kad_pengenalan_simati', 255);
            $table->string('tarikh_kematian', 255);
            $table->string('attachment_pemohon')->nullable();
            $table->string('attachment_simati')->nullable();
            $table->string('attachment_sijil')->nullable();
            $table->string('attachment_img_stored_path_pemohon')->nullable();
            $table->string('attachment_img_stored_path_simati')->nullable();
            $table->string('attachment_img_stored_path_sijil')->nullable();
            $table->string('attachment_img_mime_type_pemohon')->nullable();
            $table->string('attachment_img_mime_type_simati')->nullable();
            $table->string('attachment_img_mime_type_sijil')->nullable();
            $table->string('tempat_pembicaraan', 255)->nullable();
            $table->string('pembicaraan_date', 255)->nullable();
            $table->string('pembicaraan_time', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('applications');
    }
}
