<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePenghutangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('penghutangs', function (Blueprint $table) {
            $table->id();
            $table->string('user_id', 255);
            $table->string('application_id', 255);
            $table->string('nama', 255);
            $table->string('perihalan', 255);
            $table->string('amaun', 255);
            $table->string('alamat', 255);
            $table->string('telefon', 255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('penghutangs');
    }
}
