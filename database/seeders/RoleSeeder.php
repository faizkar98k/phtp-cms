<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['id' => 1, 'name' => 'ADMIN', 'description' => 'Admin Role', 'created_at' => now(), 'updated_at' => now()],
            ['id' => 2, 'name' => 'USER', 'description' => 'User Role', 'created_at' => now(), 'updated_at' => now()],
        ]);
    }
}
