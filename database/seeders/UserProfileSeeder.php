<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserProfileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_profiles')->insert([
            [
                'user_id' => 1,
                'user_type' => 'admin',
                'first_name' => 'System',
                'last_name' => 'Admin',
                'phone_no' => '+60183422593',
                'address' => 'Y24 Kampung Baru Permatang Pauh Selangor Darul Ehsan',
                'created_at' => now(), 'updated_at' => now()
            ],
            [
                'user_id' => 2,
                'user_type' => 'user',
                'first_name' => 'Khalid',
                'last_name' => 'Walid',
                'phone_no' => '+60183422593',
                'address' => 'Y24 Kampung Baru Permatang Pauh Selangor Darul Ehsan',
                'created_at' => now(), 'updated_at' => now()
            ],


        ]);
    }
}
