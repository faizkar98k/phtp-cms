@extends('layouts.app')

@section('content')
<div class="container-lg d-flex justify-content-center">
    <div class="card col-8 p-3">
        <h3>Permohonan Tidak Lengkap</h3>
        <form action="{{route('update_email_form')}}" method="POST">
            @csrf
            <div class="form-group">
                <input type="hidden" name="application_id" value="{{$application_id}}">
                <label for="">Description</label>
                <textarea name="description" class="form-control" id="" cols="20" rows="5" placeholder="Sila nyatakan dengan lebih terperinci permohonan ini tidak lengkap.... "></textarea>
            </div>
            <div>
                <button type="submit" class="btn btn-primary">Hantar</button>
            </div>
        </form>
    </div>


</div>
@endsection