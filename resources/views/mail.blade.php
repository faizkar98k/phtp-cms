<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>


<body style="font-family:Arial, Helvetica, sans-serif;">
    <div style="font-size: 18px;background-color: #EDF2F7;padding:68px;width:100%">
        <div style="background-color: white;padding:16px;width: 500px;">
            <p>Hi, {{$name}}</p>
            <p>{{$description}}</p>
            <p>Terima Kasih,</p>
            <div>Pembahagian Harta Pusaka Admin</div>
        </div>

    </div>

</body>

</html>