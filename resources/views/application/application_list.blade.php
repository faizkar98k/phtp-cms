@extends('layouts.app')

@section('content')
@if($message = session()->get('success'))
<div class="alert alert-success" role="alert">
    {{ $message }}
</div>
@endif
<div class="container-lg">
    <div class="mb-5">
        <h4>Senarai Permohonan</h4>
    </div>
    <div>
        <form action="{{route('filter_application')}}">
            @csrf
            <div class="d-flex justify-content-center align-items-center">
                <div>Reference</div>
                <input type="text" class="form-control ml-2 mr-2" name="reference_id">
                <div>Status</div>
                <select class="form-control ml-2 mr-2" onchange="filter(this.value)" name="status" id="status">
                    <option value="null">Semua Status</option>
                    <option value="Dalam Proses">Dalam Proses</option>
                    <option value="Tidak Lengkap">Tidak Lengkap</option>
                    <option value="Lengkap">Lengkap</option>
                    <option value="Pembicaraan">Pembicaraan</option>
                    <option value="Selesai">Selesai</option>
                </select>

                <button type="submit" name="btn" value="search" class="btn btn-primary mr-2">Search</button>
                <button type="submit" name="btn" value="reset" class="btn btn-outline-primary">Reset</button>
            </div>
        </form>
    </div>
    <table class="table table-striped mt-3 border">
        <thead>
            <tr class="bg-primary text-white">
                <th scope="col">Reference</th>
                <th scope="col">Nama Pemohon</th>
                <th scope="col">Nama Si Mati</th>
                <th scope="col">Tarikh Mohon</th>
                <th scope="col">Status</th>
                <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @foreach($applications as $application)
            <tr>
                <th scope="row">{{$application->reference_id}}</th>
                <td>
                    <div>{{$application->nama_pemohon}}</div>
                    <div class="text-secondary" style="font-size: 12px;">
                        <div>Kad Pengenalan : {{$application->kad_pengenalan_pemohon}}</div>
                        <div>No Telefon : {{$application->telefon}}</div>
                    </div>
                </td>
                <td>
                    <div>{{$application->nama_simati}}</div>
                    <div class="text-secondary" style="font-size: 12px;">
                        <div>Kad Pengenalan : {{$application->kad_pengenalan_simati}}</div>
                        <div>Tarikh Kematian : {{$application->tarikh_kematian}}</div>
                    </div>
                </td>
                <td>{{$application->created_at->format('d F Y')}}</td>
                <td>
                    @if($application->status == 'belum disemak')
                    <div class="badge badge-danger text-uppercase">{{$application->status}}</div>
                    @endif
                    @if($application->status == 'dalam proses')
                    <div class="badge badge-info text-uppercase">{{$application->status}}</div>
                    @endif
                    @if($application->status == 'tidak lengkap')
                    <div class="badge badge-warning text-uppercase">{{$application->status}}</div>
                    @endif
                    @if($application->status == 'lengkap')
                    <div class="badge badge-success text-uppercase">{{$application->status}}</div>
                    @endif
                    @if($application->status == 'pembicaraan')
                    <div class="badge badge-secondary text-uppercase">{{$application->status}}</div>
                    @endif
                    @if($application->status == 'selesai')
                    <div class="badge badge-success text-uppercase">{{$application->status}}</div>
                    @endif
                </td>
                <td>
                    <div class="dropdown">

                        <svg xmlns="http://www.w3.org/2000/svg" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false" width="16" height="16" fill="currentColor" class="bi bi-three-dots-vertical" viewBox="0 0 16 16">
                            <path d="M9.5 13a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0zm0-5a1.5 1.5 0 1 1-3 0 1.5 1.5 0 0 1 3 0z" />
                        </svg>

                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                            <li><a class="dropdown-item" href="{{route('application_details', ['id' => $application->id ])}}">Review</a></li>
                            <li><a class="dropdown-item" onclick="return confirm('Adakah anda ingin menukar status kepada DALAM PROSES?')" href="{{route('application_list.inProcess',['id'=>$application->id,'reference_id'=>$application->reference_id])}}">Dalam Proses</a></li>
                            <li><a class="dropdown-item" href="{{route('email_form',['id'=>$application->id])}}">Tidak Lengkap</a></li>
                            <li><a class="dropdown-item" onclick="return confirm('Adakah anda ingin menukar status kepada LENGKAP?')" href="{{route('application_list.complete',['id'=>$application->id,'reference_id'=>$application->reference_id])}}">Lengkap</a></li>
                            <li><a class="dropdown-item" onclick="return confirm('Adakah anda ingin menukar status kepada PEMBICARAAN?')" href="{{route('pembicaraan_form',['id'=>$application->id,'reference_id'=>$application->reference_id])}}">Pembicaraan</a></li>
                            <li><a class="dropdown-item" onclick="return confirm('Adakah anda ingin menukar status kepada SELESAI?')" href="{{route('application_list.selesai',['id'=>$application->id,'reference_id'=>$application->reference_id])}}">Selesai</a></li>

                        </ul>
                    </div>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection

@section('script')
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.1/dist/umd/popper.min.js" integrity="sha384-SR1sx49pcuLnqZUnnPwx6FCym0wLsk5JZuNx2bPPENzswTNFaQU1RDvt3wT4gWFG" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.min.js" integrity="sha384-j0CNLUeiqtyaRmlzUHCPZ+Gy5fQu0dQ6eZ/xAww941Ai1SxSY+0EQqNXNE6DZiVc" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

@endsection