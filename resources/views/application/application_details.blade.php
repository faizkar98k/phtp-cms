@extends('layouts.app')

<style>
    .card {
        border: 1px solid #eeeeee !important;
        padding: 16px;
        border-radius: 0px !important;
    }

    .sub-container {
        margin-top: 16px;
    }

    .text-header {
        font-size: 18px;
        color: #28527a;
        font-weight: bold;
    }

    .text-title {
        color: grey;
    }

    .text-subtitle {
        font-weight: bold;
        font-size: 14px;
    }
</style>

@section('content')
<div class="container-lg">
    <div class="d-flex justify-content-end align-items-end mb-4">
        <a href="{{route('check_list',['application_id' => $application->id])}}" class="btn btn-primary">Senarai Semak</a>
    </div>
    <div class="card shadow-none">
        <div class="text-header">Maklumat Permohonan</div>
        <div class="sub-container d-flex">
            <div class="col-5 p-0">
                <div class="text-title">Reference</div>
                <div class="text-subtitle">#{{$application->reference_id}}</div>
            </div>
            <div class="">
                <div class="text-title">Tarikh Pemohonan</div>
                <div class="text-subtitle">{{$application->created_at->format('d F Y')}}</div>
            </div>
        </div>
        <div class="sub-container">
            <div class="text-title">Masa Permohonan</div>
            <div class="text-subtitle">{{$application->created_at->format('h:i A')}}</div>
        </div>
    </div>

    <div class="card shadow-none">
        <div class="text-header">Maklumat Pemohon</div>
        <div class="sub-container d-flex">
            <div class="col-5 p-0">
                <div class="text-title">Taraf Kepentingan</div>
                <div class="text-subtitle">{{$application->taraf_kepentingan}}</div>
            </div>
            <div class="">
                <div class="text-title">Telefon</div>
                <div class="text-subtitle">{{$application->telefon}}</div>
            </div>
        </div>
        <div class="sub-container d-flex">
            <div class="col-5 p-0">
                <div class="text-title">Nama</div>
                <div class="text-subtitle">{{$application->nama_pemohon}}</div>
            </div>
            <div class="">
                <div class="text-title">Alamat</div>
                <div class="text-subtitle">{{$application->alamat_pemohon}}</div>
            </div>
        </div>
        <div class="sub-container">
            <div class="text-title">No Kad Pengenalan</div>
            <div class="text-subtitle">{{$application->kad_pengenalan_pemohon}}</div>
        </div>
        <div class="sub-container">
            <div class="text-title">Hubungan dengan simati</div>
            <div class="text-subtitle">{{$application->hubungan_pemohon}}</div>
        </div>
        <hr>
        <div class="text-header">Lampiran Pemohon</div>
        <div class="sub-container">
            <div class="text-title">Kad Pengenalan</div>
            <img width="150px" height="150px" src="{{$application->attachment_img_stored_path_pemohon}}" alt="kad pengenalan pemohon">
        </div>
    </div>
    <div class="card shadow-none mt-2">
        <div class="text-header">Maklumat Simati</div>
        <div class="sub-container">
            <div class="text-title">Nama</div>
            <div class="text-subtitle">{{$application->nama_simati}}</div>
        </div>
        <div class="sub-container">
            <div class="text-title">No Kad Pengenalan</div>
            <div class="text-subtitle">{{$application->kad_pengenalan_simati}}</div>
        </div>
        <div class="sub-container">
            <div class="text-title">Tarikh Kematian</div>
            <div class="text-subtitle">{{$application->tarikh_kematian}}</div>
        </div>
        <hr>
        <div class="text-header">Lampiran Simati</div>
        <div class="sub-container d-flex">
            <div>
                <div class="text-title">Kad Pengenalan</div>
                <img width="150px" height="150px" src="{{$application->attachment_img_stored_path_simati}}" alt="kad pengenalan simati">
            </div>
            <div class="ml-3">
                <div class="text-title">Sijil Kematian</div>
                <img width="150px" height="150px" src="{{$application->attachment_img_stored_path_sijil}}" alt="kad pengenalan simati">
            </div>
        </div>
    </div>

    <div class="row p-0 m-0 d-flex justify-content-between">
        <div class="card shadow-none mt-2 col-6">
            <div class="text-header">Maklumat Waris</div>
            @foreach($benificiaries as $key => $benificiary)
            <div>
                <div class="mt-2"><b>Waris #{{++$key}}</b></div>
                <div class="sub-container">
                    <div class="text-title">Nama</div>
                    <div class="text-subtitle">{{$benificiary->nama_waris}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">No Kad Pengenalan</div>
                    <div class="text-subtitle">{{$benificiary->kad_pengenalan_waris}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">Hubungan dengan si mati</div>
                    <div class="text-subtitle">{{$benificiary->hubungan_waris}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">Telefon</div>
                    <div class="text-subtitle">{{$benificiary->telefon}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">Alamat</div>
                    <div class="text-subtitle">{{$benificiary->alamat_waris}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">Lampiran</div>
                    <img width="150px" height="150px" src="{{$benificiary->attachment_img_stored_path}}" alt="kad pengenalan waris">
                </div>
            </div>
            <hr>

            @endforeach
        </div>
        <div class="card shadow-none mt-2 col-6 border-left-0">
            <div class="text-header">Maklumat Harta</div>
            @foreach($inheritances as $key => $inheritance)
            <div>
                <div class="mt-2"><b>Harta #{{++$key}}</b></div>
                <div class="sub-container">
                    <div class="text-title">No. Lot</div>
                    <div class="text-subtitle">{{$inheritance->no_lot}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">No. Hak Milik</div>
                    <div class="text-subtitle">{{$inheritance->no_hak_milik}}</div>
                </div>

                <div class="sub-container">
                    <div class="text-title">Alamat</div>
                    <div class="text-subtitle">{{$inheritance->alamat}}</div>
                </div>
                <div class="sub-container">
                    <div class="text-title">Lampiran</div>
                    <img width="150px" height="150px" src="{{$benificiary->attachment_img_stored_path}}" alt="kad pengenalan waris">
                </div>
            </div>
            <hr>
            @endforeach
        </div>
    </div>
</div>
@endsection