@extends('layouts.app')


@section('content')

<style>
    .card-container {
        padding: 32px;
        background-color: white;
        border-radius: 30px;
    }

    .square {
        border-radius: 6px;
        width: 20px;
        height: 20px;
        margin-top: 2px;
    }

    .square-1 {
        background-color: #574B90;
    }

    .square-2 {
        background-color: #778BEB;
    }

    .square-3 {
        background-color: #EB8686;
    }

    .square-4 {
        background-color: #F7D794;
    }

    .square-5 {
        background-color: #e93b81;
    }

    .square-6 {
        background-color: #ff7b54;
    }

    .square-7 {
        background-color: #e5d549;
    }

    table tr td {
        border: none !important;
        padding-left: 24px !important;
        padding-right: 24px !important;

    }

    .nav-pills {
        border-radius: 0 !important;
    }

    li .active {
        background-color: #778BEB !important;
    }

    .nav-pills>.active>a:focus {
        background-color: #778BEB !important;
        color: white !important;
    }

    .register-btn {
        text-decoration: none;
        color: white !important;
        background-color: #F5CD7A;
        border-radius: 15px !important;
    }

    @media screen and (max-width: 450px) {


        .header {
            text-align: center;
        }

        .register-btn {
            width: 100%;
        }
    }
</style>

@if (Auth::user()->hasRole('ADMIN'))
<div class="container-fluid">
    <h1 class="text-center mb-5">Selamat Datang, {{Auth::user()->name}}</h1>
    <div class="row p-0 m-0 mt-sm-2 ">
        <div class="col-lg-6 col-sm-12 p-2">
            <div class="card-container" style="height: 480px;">
                <div><b>Jumlah Permohonan</b></div>
                <div class="d-flex justify-content-between align-items-center mt-4">
                    <div class="pie-chart-container m-0" style="height:30vh; width:30vw;">
                        <canvas id="myChart"></canvas>
                    </div>

                    <table class="ml-5">
                        <tr class="">
                            <td class="p-3">
                                <div class="square square-1"></div>
                            </td>
                            <td>Jumlah Permohonan</td>
                            <td id="totalApplication">0</td>
                        </tr>
                        <tr class="">
                            <td class="p-3">
                                <div class="square square-2"></div>
                            </td>
                            <td> Dalam Proses</td>
                            <td id="applicationProcess">0</td>
                        </tr>
                        <tr class="">
                            <td class="p-3">
                                <div class="square square-3"></div>
                            </td>
                            <td> Lengkap</td>
                            <td id="applicationComplete">0</td>
                        </tr>
                        <tr class="">
                            <td class="p-3">
                                <div class="square square-4"></div>
                            </td>
                            <td> Tidak Lengkap</td>
                            <td id="applicationIncomplete">0</td>
                        </tr>
                        <tr class="">
                            <td class="p-3">
                                <div class="square square-5"></div>
                            </td>
                            <td> Belum Disemak</td>
                            <td id="applicationBelumDisemak">0</td>
                        </tr>
                        <tr class=" pb-3">
                            <td class="p-3">
                                <div class="square square-6"></div>
                            </td>
                            <td>Pembicaraan </td>
                            <td id="applicationPembicaraan">0</td>
                        </tr>
                        <tr class=" pb-3">
                            <td class="p-3">
                                <div class="square square-7"></div>
                            </td>
                            <td>Selesai</td>
                            <td id="applicationSelesai">0</td>
                        </tr>
                    </table>


                </div>
            </div>
        </div>
        <div class="col-lg-6 col-sm-12 p-2">
            <div class="card-container" style="height: 480px;">
                <div class="d-flex justify-content-between">
                    <b>Jumlah Permohonan Bulanan</b>
                    <div class="btn-group btn-group-toggle m-0 p-0" data-toggle="buttons">
                        <ul role="tablist" class="nav nav-pills font-weight-bold justify-content-center">
                            <li data-toggle="tab" role="tablist" aria-expanded="true" class="active">
                                <a data-toggle="tab" role="tablist" href="#tab1" class="nav-link active" aria-expanded="true">
                                    <div>
                                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Daily</span>
                                    </div>
                                </a>
                            </li>
                            <li data-toggle="tab" role="tablist" aria-expanded="true" class="active">
                                <a data-toggle="tab" role="tablist" href="#tab2" class="nav-link">
                                    <div>
                                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Weekly</span>
                                    </div>
                                </a>
                            </li>
                            <li data-toggle="tab" role="tablist" aria-expanded="true" class="active">
                                <a data-toggle="tab" role="tablist" href="#tab3" class="nav-link">
                                    <div>
                                        <span class="d-none d-sm-block d-md-block d-lg-block d-xl-block">Monthly</span>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content tab-space tab-subcategories">
                    <div class="tab-pane active" id="tab1" aria-expanded="true">
                        <div class="d-flex justify-content-between align-items-center mt-4">
                            <div class="pie-chart-container" style="position:relative;height:30vh; width:30vw;">
                                <canvas id="myChart1" style="height:fit-content; width:fit-content;"></canvas>
                            </div>
                            <div>
                                <table class="">
                                    <tr class="">
                                        <td class="p-3">
                                            <div class="square square-1"></div>
                                        </td>
                                        <td>Jumlah Permohonan</td>
                                        <td id="totalApplicationToday">0</td>
                                    </tr>
                                    <tr class="">
                                        <td class="p-3">
                                            <div class="square square-2"></div>
                                        </td>
                                        <td> Dalam Proses</td>
                                        <td id="applicationTodayProcess">0</td>
                                    </tr>
                                    <tr class="">
                                        <td class="p-3">
                                            <div class="square square-3"></div>
                                        </td>
                                        <td> Lengkap</td>
                                        <td id="applicationTodayComplete">0</td>
                                    </tr>
                                    <tr class="">
                                        <td class="p-3">
                                            <div class="square square-4"></div>
                                        </td>
                                        <td> Tidak Lengkap</td>
                                        <td id="applicationTodayIncomplete">0</td>
                                    </tr>
                                    <tr class="">
                                        <td class="p-3">
                                            <div class="square square-5"></div>
                                        </td>
                                        <td> Belum Disemak</td>
                                        <td id="applicationTodayBelumDisemak">0</td>
                                    </tr>
                                    <tr class=" pb-3">
                                        <td class="p-3">
                                            <div class="square square-6"></div>
                                        </td>
                                        <td>Pembicaraan </td>
                                        <td id="applicationTodayPembicaraan">0</td>
                                    </tr>
                                    <tr class=" pb-3">
                                        <td class="p-3">
                                            <div class="square square-7"></div>
                                        </td>
                                        <td>Selesai</td>
                                        <td id="applicationTodaySelesai">0</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab2" aria-expanded="true">
                        <div class="d-flex justify-content-between align-items-center mt-4">
                            <div class="pie-chart-container m-0" style="position:relative;height:30vh; width:30vw;">
                                <canvas id="myChart2" style="height:fit-content; width:fit-content;"></canvas>
                            </div>
                            <table class="">
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-1"></div>
                                    </td>
                                    <td>Jumlah Permohonan</td>
                                    <td id="totalApplicationWeekly">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-2"></div>
                                    </td>
                                    <td> Dalam Proses</td>
                                    <td id="applicationWeeklyProcess">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-3"></div>
                                    </td>
                                    <td> Lengkap</td>
                                    <td id="applicationWeeklyComplete">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-4"></div>
                                    </td>
                                    <td> Tidak Lengkap</td>
                                    <td id="applicationWeeklyIncomplete">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-5"></div>
                                    </td>
                                    <td> Belum Disemak</td>
                                    <td id="applicationWeeklyBelumDisemak">0</td>
                                </tr>
                                <tr class=" pb-3">
                                    <td class="p-3">
                                        <div class="square square-6"></div>
                                    </td>
                                    <td>Pembicaraan </td>
                                    <td id="applicationWeeklyPembicaraan">0</td>
                                </tr>
                                <tr class=" pb-3">
                                    <td class="p-3">
                                        <div class="square square-7"></div>
                                    </td>
                                    <td>Selesai</td>
                                    <td id="applicationWeeklySelesai">0</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="tab-pane" id="tab3" aria-expanded="true">
                        <div class="d-flex justify-content-between align-items-center mt-4">
                            <div class="pie-chart-container" style="position:relative;height:30vh; width:30vw;">
                                <canvas id="myChart3" style="height:fit-content; width:fit-content;"></canvas>
                            </div>
                            <table class="">
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-1"></div>
                                    </td>
                                    <td>Jumlah Permohonan</td>
                                    <td id="totalApplicationMonthly">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-2"></div>
                                    </td>
                                    <td> Dalam Proses</td>
                                    <td id="applicationMonthlyProcess">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-3"></div>
                                    </td>
                                    <td> Lengkap</td>
                                    <td id="applicationMonthlyComplete">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-4"></div>
                                    </td>
                                    <td> Tidak Lengkap</td>
                                    <td id="applicationMonthlyIncomplete">0</td>
                                </tr>
                                <tr class="">
                                    <td class="p-3">
                                        <div class="square square-5"></div>
                                    </td>
                                    <td> Belum Disemak</td>
                                    <td id="applicationMonthlyBelumDisemak">0</td>
                                </tr>
                                <tr class=" pb-3">
                                    <td class="p-3">
                                        <div class="square square-6"></div>
                                    </td>
                                    <td>Pembicaraan </td>
                                    <td id="applicationMonthlyPembicaraan">0</td>
                                </tr>
                                <tr class=" pb-3">
                                    <td class="p-3">
                                        <div class="square square-7"></div>
                                    </td>
                                    <td>Selesai</td>
                                    <td id="applicationMonthlySelesai">0</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js" integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ==" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
<script type="text/javascript">
    this.isLoading = true;
    axios.get('{!! route("total_application", request()->toArray()) !!}').then((res) => {
        this.totalApplication = res.data.totalApplication;
        this.applicationBelumDisemak = res.data.applicationBelumDisemak;
        this.applicationIncomplete = res.data.applicationIncomplete;
        this.applicationComplete = res.data.applicationComplete;
        this.applicationPembicaraan = res.data.applicationPembicaraan;
        this.applicationProcess = res.data.applicationProcess;
        this.applicationSelesai = res.data.applicationSelesai;

        var xValues = ["Proses", "Lengkap", "Tidak Lengkap", "Belum Disemak", "Pembicaraan", "Selesai"];
        var yValues = [this.applicationProcess, this.applicationComplete, this.applicationIncomplete, this.applicationBelumDisemak, this.applicationPembicaraan, this.applicationSelesai];
        var barColors = [
            "#778BEB",
            "#EB8686",
            "#F7D794",
            "#e93b81",
            "#ff7b54",
            '#e5d549',
        ];

        new Chart("myChart", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false,
                },
                responsive: true,
                maintainAspectRatio: false,

            }
        });
        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
        document.getElementById("applicationPembicaraan").innerHTML = this.applicationPembicaraan;
        document.getElementById("applicationBelumDisemak").innerHTML = this.applicationBelumDisemak;
        document.getElementById("applicationIncomplete").innerHTML = this.applicationIncomplete;
        document.getElementById("applicationComplete").innerHTML = this.applicationComplete;
        document.getElementById("applicationProcess").innerHTML = this.applicationProcess;
        document.getElementById("totalApplication").innerHTML = this.totalApplication;
        document.getElementById("applicationSelesai").innerHTML = this.applicationSelesai;

    }).finally(() => {
        this.isLoading = false;
    })

    //TODAY APPLICATION
    axios.get('{!! route("total_application_today", request()->toArray()) !!}').then((res) => {
        this.totalApplicationToday = res.data.totalApplicationToday;
        this.applicationTodayBelumDisemak = res.data.applicationTodayBelumDisemak;
        this.applicationTodayIncomplete = res.data.applicationTodayIncomplete;
        this.applicationTodayComplete = res.data.applicationTodayComplete;
        this.applicationTodayPembicaraan = res.data.applicationTodayPembicaraan;
        this.applicationTodayProcess = res.data.applicationTodayProcess;
        this.applicationTodaySelesai = res.data.applicationTodaySelesai;

        var xValues = ["Proses", "Lengkap", "Tidak Lengkap", "Belum Disemak", "Pembicaraan", "Selesai"];
        var yValues = [this.applicationTodayProcess, this.applicationTodayComplete, this.applicationTodayIncomplete, this.applicationTodayBelumDisemak, this.applicationTodayPembicaraan, this.applicationTodaySelesai];
        var barColors = [
            "#778BEB",
            "#EB8686",
            "#F7D794",
            "#e93b81",
            "#ff7b54",
            "#e5d549"
        ];


        new Chart("myChart1", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
            }
        });
        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
        document.getElementById("applicationTodayPembicaraan").innerHTML = this.applicationTodayPembicaraan;
        document.getElementById("applicationTodayBelumDisemak").innerHTML = this.applicationTodayBelumDisemak;
        document.getElementById("applicationTodayIncomplete").innerHTML = this.applicationTodayIncomplete;
        document.getElementById("applicationTodayComplete").innerHTML = this.applicationTodayComplete;
        document.getElementById("applicationTodayProcess").innerHTML = this.applicationTodayProcess;
        document.getElementById("totalApplicationToday").innerHTML = this.totalApplicationToday;
        document.getElementById("applicationTodaySelesai").innerHTML = this.applicationTodaySelesai;

    }).finally(() => {
        this.isLoading = false;
    })

    //WEEKLY APPLICATION
    axios.get('{!! route("total_application_weekly", request()->toArray()) !!}').then((res) => {
        this.totalApplicationWeekly = res.data.totalApplicationWeekly;
        this.applicationWeeklyBelumDisemak = res.data.applicationWeeklyBelumDisemak;
        this.applicationWeeklyIncomplete = res.data.applicationWeeklyIncomplete;
        this.applicationWeeklyComplete = res.data.applicationWeeklyComplete;
        this.applicationWeeklyPembicaraan = res.data.applicationWeeklyPembicaraan;
        this.applicationWeeklyProcess = res.data.applicationWeeklyProcess;
        this.applicationWeeklySelesai = res.data.applicationWeeklySelesai;

        var xValues = ["Proses", "Lengkap", "Tidak Lengkap", "Belum Disemak", "Pembicaraan", "Selesai"];
        var yValues = [this.applicationWeeklyProcess, this.applicationWeeklyComplete, this.applicationWeeklyIncomplete, this.applicationWeeklyBelumDisemak, this.applicationWeeklyPembicaraan, this.applicationWeeklySelesai];
        var barColors = [
            "#778BEB",
            "#EB8686",
            "#F7D794",
            "#e93b81",
            "#ff7b54",
            "#e5d549"
        ];


        new Chart("myChart2", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
            }
        });
        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
        document.getElementById("applicationWeeklyPembicaraan").innerHTML = this.applicationWeeklyPembicaraan;
        document.getElementById("applicationWeeklyBelumDisemak").innerHTML = this.applicationWeeklyBelumDisemak;
        document.getElementById("applicationWeeklyIncomplete").innerHTML = this.applicationWeeklyIncomplete;
        document.getElementById("applicationWeeklyComplete").innerHTML = this.applicationWeeklyComplete;
        document.getElementById("applicationWeeklyProcess").innerHTML = this.applicationWeeklyProcess;
        document.getElementById("totalApplicationWeekly").innerHTML = this.totalApplicationWeekly;
        document.getElementById("applicationWeeklySelesai").innerHTML = this.applicationWeeklySelesai;

    }).finally(() => {
        this.isLoading = false;
    })

    //MONTHLY APPLICATION
    axios.get('{!! route("total_application_monthly", request()->toArray()) !!}').then((res) => {
        this.totalApplicationMonthly = res.data.totalApplicationMonthly;
        this.applicationMonthlyBelumDisemak = res.data.applicationMonthlyBelumDisemak;
        this.applicationMonthlyIncomplete = res.data.applicationMonthlyIncomplete;
        this.applicationMonthlyComplete = res.data.applicationMonthlyComplete;
        this.applicationMonthlyPembicaraan = res.data.applicationMonthlyPembicaraan;
        this.applicationMonthlyProcess = res.data.applicationMonthlyProcess;
        this.applicationMonthlySelesai = res.data.applicationMonthlySelesai;

        var xValues = ["Proses", "Lengkap", "Tidak Lengkap", "Belum Disemak", "Pembicaraan", "Selesai"];
        var yValues = [this.applicationMonthlyProcess, this.applicationMonthlyComplete, this.applicationMonthlyIncomplete, this.applicationMonthlyBelumDisemak, this.applicationMonthlyPembicaraan, this.applicationMonthlySelesai];
        var barColors = [
            "#778BEB",
            "#EB8686",
            "#F7D794",
            "#e93b81",
            "#ff7b54",
            "#e5d549"
        ];


        new Chart("myChart3", {
            type: "doughnut",
            data: {
                labels: xValues,
                datasets: [{
                    backgroundColor: barColors,
                    data: yValues
                }]
            },
            options: {
                legend: {
                    display: false
                },
                responsive: true,
                maintainAspectRatio: false,
            }
        });
        $(document).ready(function() {
            demo.initDashboardPageCharts();
        });
        document.getElementById("applicationMonthlyPembicaraan").innerHTML = this.applicationMonthlyPembicaraan;
        document.getElementById("applicationMonthlyBelumDisemak").innerHTML = this.applicationMonthlyBelumDisemak;
        document.getElementById("applicationMonthlyIncomplete").innerHTML = this.applicationMonthlyIncomplete;
        document.getElementById("applicationMonthlyComplete").innerHTML = this.applicationMonthlyComplete;
        document.getElementById("applicationMonthlyProcess").innerHTML = this.applicationMonthlyProcess;
        document.getElementById("totalApplicationMonthly").innerHTML = this.totalApplicationMonthly;
        document.getElementById("applicationMonthlySelesai").innerHTML = this.applicationMonthlySelesai;

    }).finally(() => {
        this.isLoading = false;
    })
</script>
@endpush