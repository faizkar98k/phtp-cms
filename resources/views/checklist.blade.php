@extends('layouts.app')


@section('content')

<div class="container-lg">
    <div class="card p-3">
        <div class="text-center">
            <h3> SENARAI SEMAK PERMOHONAN PETISYEN DI BAWAH SEKSYEN 8 (BORANG A)AKTA HARTA PUSAKA KECIL (PEMBAHAGIAN) 1955[AKTA98]
            </h3>
        </div>
        <div class="mb-4 text-center">
            <h4>Senarai Semak #{{$application->reference_id}}</h4>
        </div>
        <div class="mb-3">Dokumen yang perlu dikemukakan adalah seperti berikut:</div>
        @foreach($checklists as $key => $checklist)
        <div class="d-flex align-items-center mb-4">
            <div class="mr-3 border border-darken-1 rounded">
                @if($checklist->status == 1)
                <svg xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="green" class="bi bi-check" viewBox="0 0 16 16">
                    <path d="M10.97 4.97a.75.75 0 0 1 1.07 1.05l-3.99 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.267.267 0 0 1 .02-.022z" />
                </svg>
                @else
                <div style="width: 32px; height:32px"></div>
                @endif
            </div>
            <div class="">{{ $checklist->title}}</div>
        </div>
        @endforeach
    </div>
</div>

@endsection