@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-header">
            <h4>Temu Janji</h4>
        </div>
        <div class="col card-body">
            <div>
                Nama : <b>{{$appointment->name}}</b>
            </div>
            <div class="mt-1">
                Emel : <b>{{$appointment->email}}</b>
            </div>
            <div class="mt-1">
                Status : <b class="badge badge-warning text-uppercase">{{$appointment->status}}</b>
            </div>
            <div class="mt-1">
                Tarikh : <b>{{$appointment->appointment_date}}</b>
            </div>
            <div class="mt-1">
                Tujuan Temu Janji : <b>{{$appointment->appointment_purpose}}</b>
            </div>
            <div class="mt-1">
                Bilangan Orang : <b>{{$appointment->no_people}}</b>
            </div>
        </div>
        <div class="card-footer">
            <div class="d-flex justify-content-between">
                <a href="" class="btn btn-outline-primary kembali">Kembali</a>
                @if (Auth::user()->hasRole('ADMIN'))
                <div class="">
                    <a href="" class="btn btn-primary">Approve</a>
                    <a href="{{route('reject_appointment')}}" class="btn btn-danger">Reject</a>
                </div>
                @endif
            </div>

        </div>
    </div>
</div>
@endsection