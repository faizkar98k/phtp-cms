@extends('layouts.app')

@section('content')
<div class="container">
    <h3>Senarai Temu janji</h3>
    <div class="mt-5">
        <div class="mb-1">Jumlah : <b>{{count($appointments)}}</b></div>
        <table class="table table-striped  table-hover">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Emel</th>
                    <th scope="col">Tarikh</th>
                    <th scope="col">Status</th>
                    <th scope="col">Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach($appointments as $key => $appointment)
                <tr>
                    <th scope="row">{{++$key}}</th>
                    <td>{{$appointment->name}}</td>
                    <td>{{$appointment->email}}</td>
                    <td>{{$appointment->appointment_date}}</td>
                    <td>
                        @if($appointment->status == 'pending')
                        <div class="badge badge-warning text-uppercase">{{$appointment->status}}</div>
                        @endif
                        @if($appointment->status == 'success')
                        <div class="badge badge-success text-uppercase">{{$appointment->status}}</div>
                        @endif
                    </td>
                    <td> <a href="{{route('appointment_details',['id'=> $appointment->id])}}">View</a></td>

                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection