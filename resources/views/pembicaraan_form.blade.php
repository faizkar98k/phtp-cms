@extends('layouts.app')


@section('content')

<div class="container-lg">
    <div class="card">
        <div class="card-header">Maklumat Pembicaraan</div>
        <form action="{{route('update_pembicaraan')}}" class="p-3" method="POST">
            @csrf
            <input type="hidden" name="id" value="{{$id}}">
            <input type="hidden" name="reference_id" value="{{$reference_id}}">
            <div class="form-group">
                <label for="">Tempat Pembicaraan</label>
                <select name="place" class="form-control" id="">
                    <option value="Pejabat Harta Pusaka Kecil Segamat">Pejabat Harta Pusaka Kecil Segamat</option>
                    <option value="Unit Pembahagian Pusaka Kecil Muar">Unit Pembahagian Pusaka Kecil Muar</option>
                </select>
            </div>
            <div class="form-group">
                <label for="">Tarikh Pembicaraan</label>
                <input type="date" class="form-control @error('date') is-invalid @enderror" name="date" min="{{ Carbon\Carbon::now()->format('Y-m-d') }}">
                @error('date')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="">Masa Pembicaraan</label>
                <input type="time" class="form-control @error('time') is-invalid @enderror" name="time">
                @error('time')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="">
                <button class="btn btn-primary" type="submit">Hantar</button>
            </div>
        </form>
    </div>

</div>

@endsection