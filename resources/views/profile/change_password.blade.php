@extends('layouts.app')

@section('content')
@if($message = session()->get('success'))
<div class="alert alert-success" role="alert">
    {{ $message }}
</div>
@endif
@if($message = session()->get('warning'))
<div class="alert alert-warning" role="alert">
    {{ $message }}
</div>
@endif
<div class="container">
    <h3>Tukar Kata Laluan</h3>
    <form action="{{route('update_password')}}" method="POST">
        @csrf
        <div class="mt-5">
            <div class=" form-group">
                <label for="password">Kata Laluan Lama</label>
                <input type="password" id="password" name="password" class="form-control @error('password') is-invalid @enderror">
                @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class=" form-group">
                <label for="confirm_password">Kata Laluan Baru</label>
                <input type="password" id="confirm_password" name="confirm_password" class="form-control @error('confirm_password') is-invalid @enderror">
                @error('confirm_password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="">
                <button type="submit" class="form-control btn btn-primary">Kemaskini</button>
            </div>
        </div>

    </form>
</div>
@endsection