@extends('layouts.app')


@section('content')
<div class="container">
    @if (\Session::has('success'))
    <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
    </div>
    @endif
    <h3>Kemaskini Profil</h3>
    <form action="{{ route('update_profile') }}" method="POST">
        @csrf
        <div class="mt-5">
            <div class="form-group">
                <label for="name">Nama</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name" value="{{ old('name') ?? $profile->name}}">
                @error('name')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>

            <div class="form-group">
                <label for="">Emel</label>
                <input disabled type="text" class="form-control " value="{{$profile->email}}">
            </div>
            <div class="form-group">
                <label for="phone_no">No Telefon</label>
                <input type="text" id="phone_no" class="form-control @error('phone_no') is-invalid @enderror" value=" {{ old('phone_no') ?? $profile->profile->phone_no}}" name="phone_no">
                @error('phone_no')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="address">Alamat</label>
                <input type="text" id="address" class="form-control @error('address') is-invalid @enderror" value=" {{ old('address') ?? $profile->profile->address}}" name="address">
                @error('address')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
                @enderror
            </div>
            <div class="form-group">
                <button type="submit" class="form-control btn btn-primary">Kemaskini</button>
            </div>
        </div>
    </form>

</div>

@endsection