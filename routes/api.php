<?php

use App\Http\Controllers\api\AppointmentController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\UserController;
use App\Http\Controllers\Api\ApplicationController;
use App\Http\Controllers\Api\CheckListController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {

    Route::group(['prefix' => 'account'], function () {
        Route::post('change-password',  [UserController::class, 'changePassword']);
        Route::post('logout',  [UserController::class, 'logout']);
        Route::get('profile',  [UserController::class, 'profile']);
        Route::patch('update-profile', [UserController::class, 'updateProfile']);
    });

    Route::group(['prefix' => 'application'], function () {
        Route::post('create-application',  [ApplicationController::class, 'createApplication']);
        Route::get('application-list',  [ApplicationController::class, 'getAllApplication']);
        Route::post('application',  [ApplicationController::class, 'getApplication']);
        Route::patch('update-application',  [ApplicationController::class, 'updateApplication']);
        Route::post('create-benificiary',  [ApplicationController::class, 'createBenificiary']);
        Route::post('create-inheritance',  [ApplicationController::class, 'createInheritance']);
        Route::post('create-penghutang',  [ApplicationController::class, 'createPenghutang']);
        Route::post('create-pemiutang',  [ApplicationController::class, 'createPemiutang']);
        Route::post('inheritance',  [ApplicationController::class, 'getInheritance']);
        Route::post('delete-application',  [ApplicationController::class, 'deleteApplication']);
        Route::post('delete-benificiary',  [ApplicationController::class, 'deleteBenificiary']);
        Route::post('delete-inheritance',  [ApplicationController::class, 'deleteInheritance']);
        Route::post('delete-penghutang',  [ApplicationController::class, 'deletePenghutang']);
        Route::post('delete-pemiutang',  [ApplicationController::class, 'deletePemiutang']);
        Route::post('benificiary',  [ApplicationController::class, 'getBenificiary']);
        Route::post('inheritance',  [ApplicationController::class, 'getInheritance']);
        Route::post('update-inheritance',  [ApplicationController::class, 'updateInheritance']);
        Route::post('update-benificiary',  [ApplicationController::class, 'updateBenificiary']);
        Route::post('update-pemiutang',  [ApplicationController::class, 'updatePemiutang']);
        Route::post('update-penghutang',  [ApplicationController::class, 'updatePenghutang']);
    });

    Route::group(['prefix' => 'booking'], function () {
        Route::post('booking',  [AppointmentController::class, 'createAppointment']);
        Route::get('booking',  [AppointmentController::class, 'getAppointment']);
        Route::post('delete-booking',  [AppointmentController::class, 'deleteAppointment']);
    });

    Route::group(['prefix' => 'checklist'], function () {
        Route::post('getChecklist',  [CheckListController::class, 'getCheckList']);
        Route::post('checklist',  [CheckListController::class, 'updateCheckList']);
    });
});

Route::post('login', [UserController::class, 'login']);
Route::post('register',  [UserController::class, 'register']);
