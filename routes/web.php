<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\ApplicationController;
use App\Http\Controllers\AppointmentController;
use App\Http\Controllers\CheckListController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes(['verify' => true]);

Route::middleware(['auth', 'verified'])->group(
    function () {
        Route::get('/home', [HomeController::class, 'index'])->name('home');

        //Application
        Route::get('/application_list', [ApplicationController::class, 'index'])->name('application_list');
        Route::get('/filter_application', [ApplicationController::class, 'filterApplication'])->name('filter_application');
        Route::get('/application_details', [ApplicationController::class, 'applicationDetails'])->name('application_details');
        Route::get('/application_list.complete', [ApplicationController::class, 'complete'])->name('application_list.complete');
        Route::get('/application_list.inComplete', [ApplicationController::class, 'inComplete'])->name('application_list.inComplete');
        Route::get('/application_list.inProcess', [ApplicationController::class, 'inProcess'])->name('application_list.inProcess');
        Route::get('/application_list.selesai', [ApplicationController::class, 'selesai'])->name('application_list.selesai');
        Route::get('/pembicaraan_form', [ApplicationController::class, 'pembicaraan'])->name('pembicaraan_form');
        Route::post('/update_pembicaraan', [ApplicationController::class, 'updatePembicaraan'])->name('update_pembicaraan');
        Route::get('/total_application', [HomeController::class, 'totalApplication'])->name('total_application');
        Route::get('/total_application_today', [HomeController::class, 'totalApplicationToday'])->name('total_application_today');
        Route::get('/total_application_weekly', [HomeController::class, 'totalApplicationWeek'])->name('total_application_weekly');
        Route::get('/total_application_monthly', [HomeController::class, 'totalApplicationMonthly'])->name('total_application_monthly');

        //Email Incomplete
        Route::get('/email_form', [ApplicationController::class, 'emailForm'])->name('email_form');
        Route::post('/update_email_form', [ApplicationController::class, 'updateEmailForm'])->name('update_email_form');

        //CheckList
        Route::get('/check_list', [CheckListController::class, 'getCheckList'])->name('check_list');

        //Appointment
        Route::get('/appointment_list', [AppointmentController::class, 'index'])->name('appointment_list');
        Route::get('/appointment_details', [AppointmentController::class, 'appointmentDetails'])->name('appointment_details');
        Route::get('/reject_appointment', [AppointmentController::class, 'rejectAppointment'])->name('reject_appointment');

        //User
        Route::get('/edit_profile', [UserController::class, 'editProfile'])->name('edit_profile');
        Route::get('/change_password', [UserController::class, 'changePassword'])->name('change_password');
        Route::post('/update_profile', [UserController::class, 'updateProfile'])->name('update_profile');
        Route::post('/update_password', [UserController::class, 'updatePassword'])->name('update_password');
    }
);
