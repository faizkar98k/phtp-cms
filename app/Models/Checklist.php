<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checklist extends Model
{
    use HasFactory;

    const STATUS_COMPLETE = 'complete';
    const STATUS_INCOMPLETE = 'incomplete';

    protected $fillable = [
        'application_id',
        'user_id',
        'title',
    ];
}
