<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'user_type',
        'first_name',
        'last_name',
        'phone_no',
        'address',
    ];


    public function user()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
