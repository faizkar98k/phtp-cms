<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Benificiary extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'application_id',
        'nama_waris',
        'hubungan_waris',
        'kad_pengenalan_waris',
        'telefon',
        'alamat_waris',
        'attachment',
        'attachment_img_stored_path',
        'attachment_img_mime_type',
    ];

    protected  $table = 'benificiaries';
}
