<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    const STATUS_PENDING = 'pending';
    const STATUS_APPROVE = 'approve';
    const STATUS_REJECT = 'reject';

    protected $fillable = [
        'user_id',
        'status',
        'name',
        'email',
        'appointment_purpose',
        'no_people',
        'appointment_date',
    ];
}
