<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penghutang extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'application_id',
        'nama',
        'telefon',
        'alamat',
        'perihalan',
        'amaun',
    ];
}
