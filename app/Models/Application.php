<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    use HasFactory;

    const STATUS_COMPLETED = 'lengkap';
    const STATUS_INCOMPLETE = 'tidak lengkap';
    const STATUS_PEMBICARAAN = 'pembicaraan';
    const STATUS_PROCESS = 'dalam proses';
    const STATUS_BELUM_DISEMAK = 'belum disemak';
    const STATUS_SELESAI = 'selesai';

    protected $fillable = [
        'user_id',
        'reference_id',
        'status',
        'taraf_kepentingan',
        'nama_pemohon',
        'hubungan_pemohon',
        'kad_pengenalan_pemohon',
        'alamat_pemohon',
        'telefon',
        'nama_simati',
        'kad_pengenalan_simati',
        'tarikh_kematian',
        'attachment_pemohon',
        'attachment_simati',
        'attachment_sijil',
        'attachment_img_stored_path_pemohon',
        'attachment_img_stored_path_simati',
        'attachment_img_stored_path_sijil',
        'attachment_img_mime_type_pemohon',
        'attachment_img_mime_type_simati',
        'attachment_img_mime_type_sijil',
        'tempat_pembicaraan',
        'pembicaraan_date',
        'pembicaraan_time',
    ];

    public function benificiary()
    {
        return $this->hasMany('App\Models\Benificiary');
    }

    public function inheritance()
    {
        return $this->hasMany('App\Models\Inheritance');
    }

    public function penghutang()
    {
        return $this->hasMany('App\Models\Penghutang');
    }

    public function pemiutang()
    {
        return $this->hasMany('App\Models\Pemiutang');
    }
}
