<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pemiutang extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'application_id',
        'nama',
        'telefon',
        'alamat',
        'butiran',
        'jenis_hutang',
        'amaun',
    ];
}
