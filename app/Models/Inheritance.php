<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inheritance extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'application_id',
        'no_hak_milik',
        'no_lot',
        'alamat',
        'attachment',
        'attachment_img_stored_path',
        'attachment_img_mime_type',
    ];
}
