<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BenificiaryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'application_id' => 'required',
            'nama_waris' => 'required',
            'hubungan_waris' => 'required',
            'kad_pengenalan_waris' => 'required',
            'alamat_waris' => 'required',
            'telefon' => 'required',
            'attachment' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
}
