<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama_pemohon' => 'required',
            'hubungan_pemohon'  => 'required',
            'kad_pengenalan_pemohon'  => 'required',
            'alamat_pemohon'  => 'required',
            'telefon'  => 'required',
            'nama_simati'  => 'required',
            'kad_pengenalan_simati'  => 'required',
            'tarikh_kematian'  => 'required',
            'attachment_pemohon' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'attachment_simati' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
            'attachment_sijil' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

        ];
    }
}
