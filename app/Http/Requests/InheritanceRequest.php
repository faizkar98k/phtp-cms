<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InheritanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'no_hak_milik' => 'required',
            'no_lot' => 'required',
            'alamat' => 'required',
            'attachment' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',
        ];
    }
}
