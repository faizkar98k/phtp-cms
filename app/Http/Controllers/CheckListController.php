<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Checklist;
use Illuminate\Http\Request;

class CheckListController extends Controller
{

    public function getCheckList(Request $request)
    {
        $application = Application::where("id", $request->application_id)->first();
        $checklists = Checklist::where("application_id", $request->application_id)
            ->get();


        return view('checklist', compact('application', 'checklists'));
    }
}
