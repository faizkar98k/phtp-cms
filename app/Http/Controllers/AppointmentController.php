<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;

class AppointmentController extends Controller
{
    public function index()
    {
        $appointments = Appointment::get();

        return view('appointment/appointment_list', compact('appointments'));
    }

    public function appointmentDetails(Request $request)
    {
        $appointment = Appointment::where('id', $request->id)->first();
        return view('appointment/appointment_details', compact('appointment'));
    }

    public function approveAppointment(Request $request)
    {
        $appointment = Appointment::where('id', $request->id)->first();
        return view('appointment/appointment_details', compact('appointment'));
    }


    public function rejectAppointment(Request $request)
    {
        return view('email_form');
    }
}
