<?php

namespace App\Http\Controllers;

use App\Models\Application;
use App\Models\Appointment;
use App\Models\User;
use Illuminate\Http\Request;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $application = Application::get();
        // $staff = User::where()->get();
        // $user = User::where()->get();
        $applicationToday = Application::where('created_at', Carbon::now()->toDateString())->get();


        return view('home', compact('application', 'applicationToday'));
    }

    public function totalApplication()
    {
        $application = Application::all()->count();
        $applicationBelumDisemak = Application::where('status', Application::STATUS_BELUM_DISEMAK)
            ->count();
        $applicationIncomplete = Application::where('status', Application::STATUS_INCOMPLETE)
            ->count();
        $applicationComplete = Application::where('status', Application::STATUS_COMPLETED)
            ->count();

        $applicationPembicaraan = Application::where('status', Application::STATUS_PEMBICARAAN)
            ->count();
        $applicationProcess = Application::where('status', Application::STATUS_PROCESS)
            ->count();

        $applicationSelesai = Application::where('status', Application::STATUS_SELESAI)
            ->count();
        return [
            'totalApplication' => $application,
            'applicationBelumDisemak' => $applicationBelumDisemak,
            'applicationIncomplete' => $applicationIncomplete,
            'applicationComplete' => $applicationComplete,
            'applicationPembicaraan' => $applicationPembicaraan,
            'applicationProcess' => $applicationProcess,
            'applicationSelesai' => $applicationSelesai

        ];
    }

    public function totalApplicationToday()
    {

        $applicationToday = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->count();

        $applicationTodayBelumDisemak = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_BELUM_DISEMAK)
            ->count();

        $applicationTodayIncomplete = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_INCOMPLETE)
            ->count();

        $applicationTodayComplete = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_COMPLETED)
            ->count();

        $applicationTodayPembicaraan = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_PEMBICARAAN)
            ->count();

        $applicationTodayProcess = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_PROCESS)
            ->count();

        $applicationTodaySelesai = Application::whereDate('created_at', '=', Carbon::today()->toDateString())
            ->where('status', Application::STATUS_SELESAI)
            ->count();
        return [
            'totalApplicationToday' => $applicationToday,
            'applicationTodayBelumDisemak' => $applicationTodayBelumDisemak,
            'applicationTodayIncomplete' => $applicationTodayIncomplete,
            'applicationTodayComplete' => $applicationTodayComplete,
            'applicationTodayPembicaraan' => $applicationTodayPembicaraan,
            'applicationTodayProcess' => $applicationTodayProcess,
            'applicationTodaySelesai' => $applicationTodaySelesai,

        ];
    }

    public function totalApplicationWeek()
    {
        $startWeek = Carbon::now()->startOfWeek();
        $endWeek = Carbon::now()->endOfWeek();

        $applicationWeekly = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->count();

        $applicationWeeklyBelumDisemak = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_BELUM_DISEMAK)
            ->count();

        $applicationWeeklyIncomplete = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_INCOMPLETE)
            ->count();

        $applicationWeeklyComplete = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_COMPLETED)
            ->count();

        $applicationWeeklyPembicaraan = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_PEMBICARAAN)
            ->count();

        $applicationWeeklyProcess = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_PROCESS)
            ->count();
        $applicationWeeklySelesai = Application::whereBetween('created_at', [$startWeek, $endWeek])
            ->where('status', Application::STATUS_SELESAI)
            ->count();
        return [
            'totalApplicationWeekly' => $applicationWeekly,
            'applicationWeeklyBelumDisemak' => $applicationWeeklyBelumDisemak,
            'applicationWeeklyIncomplete' => $applicationWeeklyIncomplete,
            'applicationWeeklyComplete' => $applicationWeeklyComplete,
            'applicationWeeklyPembicaraan' => $applicationWeeklyPembicaraan,
            'applicationWeeklyProcess' => $applicationWeeklyProcess,
            'applicationWeeklySelesai' => $applicationWeeklySelesai

        ];
    }




    public function totalApplicationMonthly()
    {

        $applicationMonthly = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->count();

        $applicationMonthlyBelumDisemak = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_BELUM_DISEMAK)
            ->count();

        $applicationMonthlyIncomplete = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_INCOMPLETE)
            ->count();

        $applicationMonthlyComplete = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_COMPLETED)
            ->count();

        $applicationMonthlyPembicaraan = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_PEMBICARAAN)
            ->count();

        $applicationMonthlyProcess = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_PROCESS)
            ->count();

        $applicationMonthlySelesai = Application::whereMonth('created_at', '=', Carbon::now()->month)
            ->whereYear('created_at', Carbon::now()->year)
            ->where('status', Application::STATUS_SELESAI)
            ->count();
        return [
            'totalApplicationMonthly' => $applicationMonthly,
            'applicationMonthlyBelumDisemak' => $applicationMonthlyBelumDisemak,
            'applicationMonthlyIncomplete' => $applicationMonthlyIncomplete,
            'applicationMonthlyComplete' => $applicationMonthlyComplete,
            'applicationMonthlyPembicaraan' => $applicationMonthlyPembicaraan,
            'applicationMonthlyProcess' => $applicationMonthlyProcess,
            'applicationMonthlySelesai' => $applicationMonthlySelesai

        ];
    }
}
