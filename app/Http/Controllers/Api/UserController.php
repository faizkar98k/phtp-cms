<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\ProfileRequest;
use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\RegisterRequest;
use App\Models\Checklist;
use App\Models\RoleUser;
use App\Models\UserProfile;

class UserController extends BaseController
{
    public function login(LoginRequest $request)
    {

        $profile = User::where('email', $request->email)->first();

        if ($profile) {

            $user = User::where('email', $request->email)->where('status', User::STATUS_ACTIVE)->with('profile')->first();
            if (!$user) {
                abort(401, 'Invalid login email or password');
            }
            if (Hash::check($request->password, $user->password)) {

                $token = $user->createToken('token')->accessToken;
                return [
                    'user' => $user,
                    'token' => $token
                ];
            }
        }

        abort(401, 'Invalid login email or password');
    }


    public function register(RegisterRequest $request)
    {

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        UserProfile::create([
            'user_id' => $user->id,
            'user_type' => 'user',
            'first_name' => $request->name,
            'last_name' => $request->name,
            'phone_no' => $request->phone_no,
            'address' => $request->address,
        ]);


        RoleUser::create([
            'role_id' => 2,
            'user_id' => $user->id,
        ]);

        return $user;
    }

    public function logout(Request $request)
    {
        $token = $request->user()->token();
        return $token->revoke();
    }


    public function changePassword(ChangePasswordRequest $request)
    {
        $user =  User::where('id', Auth::user()->id)->firstOrFail();

        if (Hash::check($request->password, $user->password)) {

            $user->password = Hash::make($request->confirm_password);

            $user->saveOrFail();

            return $user;
        }

        abort(401, 'Invalid password');
    }

    public function profile()
    {
        $user =  User::where('id', Auth::user()->id)
            ->with('profile')
            ->firstOrFail();
        return $user;
    }

    public function updateProfile(ProfileRequest $request, User $user)
    {

        $user = User::where('id', Auth::user()->id)
            ->with('profile')
            ->firstOrFail();

        $user->name = $request->name;
        $user->profile->phone_no = $request->phone_no;
        $user->profile->address = $request->address;

        $user->saveOrFail();
        $user->profile->saveOrFail();

        return $user;
    }


    public function createCheckList(Request $request)
    {
    }
}
