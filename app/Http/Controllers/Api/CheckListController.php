<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\api\BaseController;
use App\Models\Checklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckListController extends BaseController
{
    public function getCheckList(Request $request)
    {
        return Checklist::where("user_id", Auth::user()->id)
            ->where("application_id", $request->application_id)
            ->get();
    }

    public function updateCheckList(Request $request)
    {
        $checklist = Checklist::where("user_id", Auth::user()->id)->where("id", $request->id)->firstOrFail();
        if ($checklist->status == 0) {
            $checklist->status = 1;
        } else {
            $checklist->status = 0;
        }



        $checklist->save();
    }
}
