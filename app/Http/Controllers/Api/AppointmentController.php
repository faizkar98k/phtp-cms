<?php

namespace App\Http\Controllers\api;


use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\AppointmentRequest;
use App\Models\Appointment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends BaseController
{
    public function  createAppointment(AppointmentRequest $request)
    {
        $profile = Auth::user()->profile;

        $appointment = Appointment::create([
            'user_id' => $profile->user_id,
            'status' => Appointment::STATUS_PENDING,
            'name' => $request->name,
            'email' => $request->email,
            'appointment_purpose' => $request->appointment_purpose,
            'no_people' => $request->no_people,
            'appointment_date' => $request->appointment_date,
        ]);

        return $appointment;
    }

    public function  getAppointment()
    {
        $user = Auth::user()->profile;
        $appointment = Appointment::where('user_id', $user->user_id)->get();
        return $appointment;
    }

    public function  deleteAppointment(Request $request)
    {
        $user = Auth::user()->profile;
        Appointment::where('user_id', $user->user_id)->where('id', $request->id)->delete();

        $appointment = Appointment::where('user_id', $user->user_id)->get();
        return $appointment;
    }
}
