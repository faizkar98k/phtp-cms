<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Api\BaseController;
use App\Http\Requests\ApplicationRequest;
use App\Http\Requests\BenificiaryRequest;
use App\Http\Requests\InheritanceRequest;
use App\Http\Requests\UpdateInheritanceRequest;
use App\Http\Requests\UpdateBenificiaryRequest;
use App\Http\Requests\PenghutangRequest;
use App\Http\Requests\PemiutangRequest;
use App\Http\Requests\UpdatePemiutangRequest;
use App\Http\Requests\UpdatePenghutangRequest;
use App\Models\Application;
use App\Models\Benificiary;
use App\Models\Inheritance;
use App\Models\Penghutang;
use App\Models\Pemiutang;
use App\Models\Checklist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Nexmo\Laravel\Facade\Nexmo;


class ApplicationController extends BaseController
{
    public function createApplication(ApplicationRequest $request)
    {

        function unique_code($limit)
        {
            return 'PHP' . substr(base_convert(sha1(uniqid(mt_rand())), 16, 36), 0, $limit);
        }


        $profile  = Auth::user()->profile;

        $dir_file  = '/images/profile';
        $imageName = time() . '.' . $request->attachment_pemohon->extension();
        $imageName2 = time() . '2.' . $request->attachment_simati->extension();
        $imageName3 = time() . '3.' . $request->attachment_sijil->extension();
        $pathImage = public_path($dir_file);

        $request->attachment_pemohon->move($pathImage, $imageName);
        $request->attachment_simati->move($pathImage, $imageName2);
        $request->attachment_sijil->move($pathImage, $imageName3);


        $application = Application::create([
            'user_id' => $profile->user_id,
            'reference_id' => '0',
            'status' => Application::STATUS_BELUM_DISEMAK,
            'taraf_kepentingan' => $request->taraf_kepentingan,
            'nama_pemohon' => $request->nama_pemohon,
            'hubungan_pemohon' => $request->hubungan_pemohon,
            'kad_pengenalan_pemohon' => $request->kad_pengenalan_pemohon,
            'alamat_pemohon' => $request->alamat_pemohon,
            'telefon' => $request->telefon,
            'nama_simati' => $request->nama_simati,
            'kad_pengenalan_simati' => $request->kad_pengenalan_simati,
            'tarikh_kematian' => $request->tarikh_kematian,
            'attachment_pemohon' => $imageName,
            'attachment_simati' => $imageName2,
            'attachment_sijil' => $imageName3,
            'attachment_img_stored_path_pemohon' => $dir_file . '/' . $imageName,
            'attachment_img_stored_path_simati' => $dir_file . '/' . $imageName2,
            'attachment_img_stored_path_sijil' => $dir_file . '/' . $imageName3,
            'attachment_img_mime_type_pemohon' => $request->attachment_pemohon->getClientMimeType(),
            'attachment_img_mime_type_simati' => $request->attachment_simati->getClientMimeType(),
            'attachment_img_mime_type_sijil' => $request->attachment_sijil->getClientMimeType(),
        ]);


        $application->reference_id =  "PHP" . $application->id;

        $application->saveOrFail();

        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => 'Borang permohonan waris si mati',
            'status' => false,
        ]);
        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => 'Salinan sijil kematian',
            'status' => false,
        ]);
        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => 'Salinan kad pengenalan bagi setiap waris yang berhak',
            'status' => false,
        ]);
        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => '2 salinan CARIAN (RASMI/PERSENDIRIAN) geran versi terkini (RM 75.00) + pelan',
            'status' => false,
        ]);
        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => '1 salinan buku akaun bank/Penyata KWSP/ASN dan lain-lain sekiranya ada',
            'status' => false,
        ]);

        Checklist::create([
            'user_id' => $profile->user_id,
            'application_id' => $application->id,
            'title' => 'Dua (2) Salinan Hakmilik/Geran yang lengkap beserta Pelan Tapak bagi harta tak alih',
            'status' => false,
        ]);

        return $application;
    }

    public function createBenificiary(BenificiaryRequest $request)
    {
        $profile  = Auth::user()->profile;

        $dir_file  = '/images/profile';
        $imageName = time() . '.' . $request->attachment->extension();
        $pathImage = public_path($dir_file);

        $request->attachment->move($pathImage, $imageName);

        $benificiary = Benificiary::create([
            'user_id' => $profile->user_id,
            'application_id' => $request->application_id,
            'nama_waris' => $request->nama_waris,
            'hubungan_waris' => $request->hubungan_waris,
            'kad_pengenalan_waris' => $request->kad_pengenalan_waris,
            'telefon' => $request->telefon,
            'attachment' => $imageName,
            'attachment_img_stored_path' => $dir_file . '/' . $imageName,
            'attachment_img_mime_type' => $request->attachment->getClientMimeType(),
            'alamat_waris' => $request->alamat_waris,
        ]);

        return $benificiary;
    }



    public function createInheritance(InheritanceRequest $request)
    {
        $profile  = Auth::user()->profile;

        $dir_file  = '/images/profile';
        $imageName = time() . '.' . $request->attachment->extension();
        $pathImage = public_path($dir_file);

        $request->attachment->move($pathImage, $imageName);

        $inheritance = Inheritance::create([
            'user_id' => $profile->user_id,
            'application_id' => $request->application_id,
            'no_hak_milik' => $request->no_hak_milik,
            'no_lot' => $request->no_lot,
            'alamat' => $request->alamat,
            'attachment' => $imageName,
            'attachment_img_stored_path' => $dir_file . '/' . $imageName,
            'attachment_img_mime_type' => $request->attachment->getClientMimeType(),
        ]);

        return $inheritance;
    }

    public function createPenghutang(PenghutangRequest $request)
    {
        $profile  = Auth::user()->profile;

        $penghutang = Penghutang::create([
            'user_id' => $profile->user_id,
            'application_id' => $request->application_id,
            'nama' => $request->nama,
            'perihalan' => $request->perihalan,
            'amaun' => $request->amaun,
            'alamat' => $request->alamat,
            'telefon' => $request->telefon,
        ]);

        return $penghutang;
    }

    public function createPemiutang(PemiutangRequest $request)
    {
        $profile  = Auth::user()->profile;

        $pemiutang = Pemiutang::create([
            'user_id' => $profile->user_id,
            'application_id' => $request->application_id,
            'nama' => $request->nama,
            'jenis_hutang' => $request->jenis_hutang,
            'butiran' => $request->butiran,
            'amaun' => $request->amaun,
            'alamat' => $request->alamat,
            'telefon' => $request->telefon,
        ]);

        return $pemiutang;
    }

    public function updateInheritance(UpdateInheritanceRequest $request)
    {
        $profile  = Auth::user()->profile;

        $inheritance = Inheritance::where('user_id', $profile->user_id)
            ->where('id', $request->id)
            ->firstOrFail();

        $inheritance->no_lot = $request->no_lot;
        $inheritance->no_hak_milik = $request->no_hak_milik;
        $inheritance->alamat = $request->alamat;

        if ($request->attachment != null) {
            $dir_file  = '/images/profile';
            $imageName = time() . '.' . $request->attachment->extension();
            $pathImage = public_path($dir_file);
            $request->attachment->move($pathImage, $imageName);


            $inheritance->attachment = $imageName;
            $inheritance->attachment_img_stored_path = $dir_file . '/' . $imageName;
            $inheritance->attachment_img_mime_type = $request->attachment->getClientMimeType();
        }


        $inheritance->saveOrFail();
    }

    public function updateBenificiary(UpdateBenificiaryRequest $request)
    {
        $profile  = Auth::user()->profile;

        $benificiary = Benificiary::where('user_id', $profile->user_id)
            ->where('id', $request->id)
            ->firstOrFail();

        $benificiary->nama_waris = $request->nama_waris;
        $benificiary->hubungan_waris = $request->hubungan_waris;
        $benificiary->kad_pengenalan_waris = $request->kad_pengenalan_waris;
        $benificiary->alamat_waris = $request->alamat_waris;
        $benificiary->telefon = $request->telefon;

        if ($request->attachment != null) {
            $dir_file  = '/images/profile';
            $imageName = time() . '.' . $request->attachment->extension();
            $pathImage = public_path($dir_file);
            $request->attachment->move($pathImage, $imageName);


            $benificiary->attachment = $imageName;
            $benificiary->attachment_img_stored_path = $dir_file . '/' . $imageName;
            $benificiary->attachment_img_mime_type = $request->attachment->getClientMimeType();
        }


        $benificiary->saveOrFail();
    }
    public function updatePenghutang(UpdatePenghutangRequest $request)
    {
        $profile  = Auth::user()->profile;

        $penghutang = Penghutang::where('user_id', $profile->user_id)
            ->where('id', $request->id)
            ->firstOrFail();

        $penghutang->nama = $request->nama;
        $penghutang->perihalan = $request->perihalan;
        $penghutang->amaun = $request->amaun;
        $penghutang->alamat = $request->alamat;
        $penghutang->telefon = $request->telefon;

        $penghutang->saveOrFail();
    }

    public function updatePemiutang(UpdatePemiutangRequest $request)
    {
        $profile  = Auth::user()->profile;

        $pemiutang = Pemiutang::where('user_id', $profile->user_id)
            ->where('id', $request->id)
            ->firstOrFail();

        $pemiutang->nama = $request->nama;
        $pemiutang->jenis_hutang = $request->jenis_hutang;
        $pemiutang->butiran = $request->butiran;
        $pemiutang->amaun = $request->amaun;
        $pemiutang->alamat = $request->alamat;
        $pemiutang->telefon = $request->telefon;

        $pemiutang->saveOrFail();
    }

    public function getApplication(Request $request)
    {
        $user = Auth::user()->profile;
        $application = Application::where('user_id', $user->user_id)
            ->where('id', $request->application_id)
            ->with('benificiary')
            ->with('inheritance')
            ->first();

        return $application;
    }

    public function getAllApplication()
    {
        $user = Auth::user()->profile;
        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }

    public function updateApplication(Request $request)
    {
        $user = Auth::user()->profile;

        $application = Application::where('user_id', $user->user_id)
            ->where('id', $request->id)
            ->firstOrFail();


        $application->status = Application::STATUS_BELUM_DISEMAK;

        $application->saveOrFail();


        // Nexmo::message()->send([
        //     'to'   => '601110578472',
        //     'from' => '16105552344',
        //     'text' => $application->nama_pemohon . ' telah membuat permohonan harta pusaka bagi simati yang bernama ' . $application->nama_simati . '.Tarikh Pembicaraan akan diberitahu kelak oleh pihak kami. Terima Kasih'
        // ]);

        return $application;
    }

    public function getBenificiary(Request $request)
    {
        $user = Auth::user()->profile;
        $application = Benificiary::where('user_id', $user->user_id)
            ->where('application_id', $request->application_id)
            ->get();

        return $application;
    }

    public function getInheritance(Request $request)
    {
        $user = Auth::user()->profile;
        $application = Inheritance::where('user_id', $user->user_id)
            ->where('application_id', $request->application_id)
            ->get();

        return $application;
    }



    public function deleteApplication(Request $request)
    {
        $user = Auth::user()->profile;

        Application::where('user_id', $user->user_id)
            ->where('id', $request->application_id)
            ->delete();


        Benificiary::where('user_id', $user->user_id)
            ->where('application_id', $request->application_id)
            ->delete();


        Inheritance::where('user_id', $user->user_id)
            ->where('application_id', $request->application_id)
            ->delete();


        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }

    public function deleteBenificiary(Request $request)
    {
        $user = Auth::user()->profile;

        Benificiary::where('user_id', $user->user_id)
            ->where('id', $request->id)
            ->delete();


        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }

    public function deleteInheritance(Request $request)
    {
        $user = Auth::user()->profile;

        Inheritance::where('user_id', $user->user_id)
            ->where('id', $request->id)
            ->delete();


        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }

    public function deletePenghutang(Request $request)
    {
        $user = Auth::user()->profile;

        Penghutang::where('user_id', $user->user_id)
            ->where('id', $request->id)
            ->delete();


        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }

    public function deletePemiutang(Request $request)
    {
        $user = Auth::user()->profile;

        Pemiutang::where('user_id', $user->user_id)
            ->where('id', $request->id)
            ->delete();


        $application = Application::where('user_id', $user->user_id)
            ->with('benificiary')
            ->with('inheritance')
            ->get();

        return $application;
    }
}
