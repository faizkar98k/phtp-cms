<?php

namespace App\Http\Controllers;

use App\Http\Requests\ChangePasswordRequest;
use App\Http\Requests\EditProfileRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function editProfile()
    {
        $profile = Auth::user();
        return view('profile/edit_profile', compact('profile'));
    }

    public function updateProfile(EditProfileRequest $request)
    {
        $user = User::where('id', Auth::user()->id)
            ->firstOrFail();

        $user->name = $request->name;
        $user->profile->address = $request->address;
        $user->profile->phone_no = $request->phone_no;

        $user->saveOrFail();
        $user->profile->saveOrFail();

        return redirect(route('edit_profile'))->with('success', 'Your profile have been successfully updated.');
    }

    public function changePassword()
    {
        return view('profile/change_password');
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        $user = User::where('id', Auth::user()->id)
            ->firstOrFail();

        if (Hash::check($request->password, $user->password)) {
            $user->password = Hash::make($request->password);
            $user->saveOrFail();
            return redirect(route('change_password'))->with('success', 'Your password have been successfully updated.');
        }

        return redirect(route('change_password'))->with('warning', 'Your old password is inccorect.');
    }
}
