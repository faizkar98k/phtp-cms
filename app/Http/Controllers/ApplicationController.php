<?php

namespace App\Http\Controllers;

use App\Http\Requests\PembicaraanRequest;
use App\Mail\ApplicationEmail;
use App\Models\Application;
use App\Models\Benificiary;
use App\Models\Inheritance;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Spatie\QueryBuilder\QueryBuilder;
use Symfony\Component\Console\Input\Input;

class ApplicationController extends Controller
{
    public function index()
    {
        $applications = Application::get();

        return view('application.application_list', compact('applications'));
    }

    public function emailForm(Request $request)
    {
        $application_id = $request->id;
        return view('email_form', compact('application_id'));
    }


    public function updateEmailForm(Request $request)
    {

        $application = Application::where('id', $request->application_id)->firstOrFail();
        $application->status = Application::STATUS_INCOMPLETE;
        $application->saveOrFail();


        $reference_id = $application->reference_id;
        $user_id = $application->user_id;
        $user = User::where('id', $user_id)->first();

        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Tidak Lengkap',
            'description' => 'Dukacita dimaklumkan bahawa permohonan #' . $reference_id . ' anda TIDAK LENGKAP.' . " " . $request->description,

        ];
        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $reference_id . ' telah dikemaskini kepada tidak lengkap.');
    }


    public function filterApplication(Request $request)
    {

        if ($request->btn == 'reset') {
            $applications = Application::get();
        } else if ($request->status == "null") {
            $applications =  QueryBuilder::for(Application::class)
                ->defaultSort('-created_at')
                ->where('reference_id', 'LIKE', '%' . $request->reference_id . '%')
                ->AllowedFilters(['reference_id', 'status'])
                ->get();
        } else {
            $applications =  QueryBuilder::for(Application::class)
                ->defaultSort('-created_at')
                ->where('reference_id', 'LIKE', '%' . $request->reference_id . '%')
                ->where('status', $request->status)
                ->AllowedFilters(['reference_id', 'status'])
                ->get();
        }



        return view('application.application_list', compact('applications'));
    }

    public function applicationDetails(Request $request)
    {
        $application = Application::where('id', $request->id)->first();
        $benificiaries = Benificiary::where('application_id', $request->id)->get();
        $inheritances = Inheritance::where('application_id', $request->id)->get();

        return view('application.application_details', compact('application', 'benificiaries', 'inheritances'));
    }

    public function inProcess(Request $request)
    {
        $application = Application::where('id', $request->id)->firstOrFail();

        $application->status = Application::STATUS_PROCESS;
        $application->saveOrFail();
        $user_id = $application->user_id;

        $user = User::where('id', $user_id)->first();
        $data = [
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Dalam Proses',
            'description' => 'Sukacita dimaklumkan bahawa permohonan #' . $request->reference_id . ' anda DALAM PROSES.',

        ];
        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $request->reference_id . ' telah dikemaskini kepada dalam proses.');
    }

    public function complete(Request $request)
    {
        $application = Application::where('id', $request->id)->firstOrFail();
        $application->status = Application::STATUS_COMPLETED;
        $application->saveOrFail();


        $user_id = $application->user_id;
        $user = User::where('id', $user_id)->first();

        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Lengkap',
            'description' => 'Sukacita dimaklumkan bahawa permohonan #' . $request->reference_id . ' anda telah LENGKAP. Maklumat pembicaraan akan diberitahu kelak oleh pihak kami.',
        );

        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $request->reference_id . ' telah dikemaskini kepada lengkap.');
    }

    public function inComplete(Request $request)
    {
        $application = Application::where('id', $request->id)->firstOrFail();

        $application->status = Application::STATUS_INCOMPLETE;
        $application->saveOrFail();

        $user_id = $application->user_id;
        $user = User::where('id', $user_id)->first();
        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Tidak Lengkap',
            'description' => 'Dukacita dimaklumkan bahawa permohonan #' . $request->reference_id . ' anda TIDAK LENGKAP. Sila kemaskini permohonan anda.',
        );

        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $request->reference_id . ' telah dikemaskini kepada tidak lengkap.');
    }

    public function selesai(Request $request)
    {
        $application = Application::where('id', $request->id)->firstOrFail();

        $application->status = Application::STATUS_SELESAI;
        $application->saveOrFail();

        $user_id = $application->user_id;
        $user = User::where('id', $user_id)->first();
        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Selesai',
            'description' => 'sukacita dimaklumkan bahawa permohonan #' . $request->reference_id . ' anda telah SELESAI.',
        );

        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $request->reference_id . ' telah dikemaskini kepada selesai.');
    }
    public function pembicaraan(Request $request)
    {
        $id = $request->id;
        $reference_id = $request->reference_id;
        return view('pembicaraan_form', compact('id', 'reference_id'));
    }

    public function updatePembicaraan(PembicaraanRequest $request)
    {
        $application = Application::where('id', $request->id)->firstOrFail();
        $application->status = Application::STATUS_PEMBICARAAN;
        $application->tempat_pembicaraan = $request->place;
        $application->pembicaraan_date  = $request->date;
        $application->pembicaraan_time = $request->time;

        $application->saveOrFail();

        $user_id = $application->user_id;
        $user = User::where('id', $user_id)->first();
        $data = array(
            'name' => $user->name,
            'email' => $user->email,
            'subject' => 'Permohonan Pembicaraan',
            'description' => 'Sukacita dimaklumkan bahawa permohonan #' . $request->reference_id . ' anda dalam status PEMBICARAAN dan anda dijemput untuk menghadiri sesi pembicaraan bertempat di ' . $request->place . ' pada tarikh' . Carbon::parse($request->date)->format('d M Y') . " dan masa " . Carbon::parse($request->time)->format('h:i A'),
        );

        Mail::to($data["email"])->send(new ApplicationEmail($data));

        return redirect(route('application_list'))->with('success', 'Status untuk permohonan #' . $request->reference_id . ' telah dikemaskini kepada pembicaraan.');
    }
}
